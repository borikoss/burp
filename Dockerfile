FROM ubuntu:18.04

ARG BURP_VERSION=1.7.36

RUN apt-get update && \
    apt-get install -qqy curl x11-apps openjdk-11-jdk openjdk-11-jre libxext-dev libxrender-dev libxtst-dev

RUN curl -sSL -o /burp.jar "https://portswigger.net/burp/releases/download?product=community&version=$BURP_VERSION&type=Jar"

ENV DISPLAY :0

CMD java -jar -Xmx2048M  /burp.jar
