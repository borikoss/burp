# Burp in Docker

Run Burp with GUI in Docker. Birp will run in a Host network mode.

## Usage

1. Build Docker image
```
docker-compose build
```
2. Run Burp container
```
docker-compose up -d
```
3. Stop Burp container
```
docker-compose down
```
